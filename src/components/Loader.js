import React from 'react';

const Loader = () => {
  return (
    <div className="wrapper-loader">
      <div className="loader"></div>
      <span className="waiting">please wait...</span>
    </div>
  );
}

export default Loader; 