import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import axios from 'axios'; 
import TimeConversion from './TimeConversion';
import Home from './home/Home';
import Comments from './comments/Comments';
import NoMatch from './NoMatch';

class App extends Component {  

  state = {
    topStoriesId: [],
    topStories: [],
    listLimit: 30,
    loader: true
  }

  componentDidMount() {
    this.handleData();
    this.updateList();
    document.body.addEventListener('click', this.handleSubcomments);
  }

  handleSubcomments = e => {
    const target = e.target;
    
    if (target.classList.contains('show-more-btn')) {
      const attrValue = target.getAttribute("data-kids-id").split(',');
      const nextSibling = target.nextElementSibling;
      
      if (target.classList.contains('active')) {
        return [
          target.classList.remove('active'),
          nextSibling.classList.remove('active')
        ];
      }

      nextSibling.classList.add('active');
      target.classList.add('active');
      
      if (target.classList.contains('has-comments')) {
        return null;
      }

      target.classList.add('has-comments');

      attrValue.map( subComment => 
        axios.get(
          `https://hacker-news.firebaseio.com/v0/item/${subComment}.json`
        )
        .then( response => {
          const { id, text, time, by, kids } = response.data;
  
          const printData = 
            kids ? 
              `<li>
                ${text}
                <div class="item-info">
                  <span class="author">by: ${by}</span>
                  <span class="comment-time">${TimeConversion(time)}</span>
                  <div class="sub-comments">
                    <div 
                      class="show-more-btn"
                      data-kids-id=${kids}
                    >
                      show replay
                    </div>
                    <ul class="subcomments-list">
                    </ul>
                  </div>
                </div>
              </li>`
            :
              `<li key=${id}>
                ${text}
                <div class="item-info">
                  <span class="author">by: ${by}</span>
                  <span class="comment-time">${TimeConversion(time)}</span>
                </div>
              </li>`;
          nextSibling.insertAdjacentHTML("afterbegin", printData);
        })      
      )
    }
    // end if show more btn
  }

  updateList = () => {
    window.addEventListener('scroll', e => {
      const { clientHeight, scrollTop } = e.target.scrollingElement;
      
      if ((clientHeight + scrollTop) >= document.body.offsetHeight) {
        this.setState(state => {
          return {
            listLimit: state.listLimit + 30
          }
        });
      }
    });
  }

  handleData = async () => {
    const This = this;
    await axios.get(
      `https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty`
    )
    .then(response => {
      this.setState({ topStoriesId: response.data })
      This.handleListItems(this.state.topStoriesId);
    })
    .catch(error => {
      console.log('error is ', error.response)
    }); 
  }

  handleListItems = sliceArr => {
    const length = sliceArr.length;
    sliceArr.map( (id, index) =>
      axios.get(
        `https://hacker-news.firebaseio.com/v0/item/${id}.json`
      )
      .then(response => {
        
        if ( index === parseInt(length / 1.4)) {
          this.setState({ loader: false });
        }

        this.setState(state => {
          const topStories = state.topStories.concat(response.data);
          return {
            topStories
          };
        },)
      })
    );
  }

  render() { 
    const { listLimit, topStories, loader } = this.state;
    
    return (
      <Router>
        <div className="container">
          <div className="lists-wrapper">
            <Switch> 
              <Route exact path="/">
                <Home
                  topStories={topStories}
                  listLimit={listLimit}
                  loader={loader}
                />
              </Route>

              <Route path="/comments/:id">
                <Comments />
              </Route>

              <Route path="*">
                <NoMatch />
              </Route>
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}
 
export default App;
