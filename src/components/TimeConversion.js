const timeConversion = millisec => {
  let minutes = (millisec / (1000 * 60)).toFixed(1);
  let hours = (millisec / (1000 * 60 * 60)).toFixed(1);
  let days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

  if (minutes < 60) {
    return `${parseInt(minutes)} minutes ago`;
  } else if (hours < 24) {
    return `${parseInt(hours)} hours ago`;
  } else {
    return `${parseInt(days)} days ago`;
  }
}

export default timeConversion;