import React from 'react';
import { Link } from "react-router-dom";
import TimeConversion from '../TimeConversion';

const HomeList = props => {
  return (
    <ol className="store-list">
      {props.topStories.slice(0, props.limit).map( store => 
        <li key={store.id}>
          <a 
            href={store.url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {store.title}
          </a>
          <div className="item-info">
            <span className="score-points">{store.score} points</span>
            <span className="author">by: {store.by}</span>
            <span className="comment-time">{TimeConversion(store.time)}</span>
            <span className="comments">
            <Link to={`comments/${store.id}`}>{store.descendants === 0 ? '' : `comments: ${store.descendants}`}</Link>
            </span>
          </div>
        </li>
      )}
    </ol> 
  );
}

export default HomeList;