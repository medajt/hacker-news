import React, { Component } from 'react';
import HomeList from './HomeList';
import Loader from '../Loader';

class Home extends Component {
  render() { 

    if (this.props.loader) {
      return (
        <Loader />
      );
    }

    return (
      <div>
        <HomeList 
          topStories={this.props.topStories}
          limit={this.props.listLimit}
        />
      </div>
    );
  }
}
 
export default Home;