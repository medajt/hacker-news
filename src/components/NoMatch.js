import React from 'react';

const NoMatch = () => {
  return (
    <div className="not-found">404 - Not Found</div>
  );
}

export default NoMatch;