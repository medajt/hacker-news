import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Comment from './Comment';
import axios from 'axios';
import Loader from '../Loader';

function Comments() {
  const [ kids, setKids ] = useState([]);
  let { id } = useParams();

  useEffect(() => {
    (() => {
      axios.get(
        `https://hacker-news.firebaseio.com/v0/item/${id}.json`
      )
      .then((response) => {
        const kids = response.data.kids;
        
        kids.map(comment =>
          axios.get(
            `https://hacker-news.firebaseio.com/v0/item/${comment}.json`
          )
          .then(commentResponse => {
            setKids(kids => [...kids, commentResponse.data]);
          }) 
        )
      })
    })()
  }, []);
  
  const renderComments = () => {
    return kids.map(comment => 
      <Comment 
        id={comment.id}
        text={comment.text}
        by={comment.by}
        time={comment.time}
        kids={comment.kids}
      />
    )
  }
  
  if (kids < 1) {
    return(
      <Loader />
    );
  }

  return ( 
    <div className="comments-wrapper">
      <Link to="/" className="button">go back</Link>
      <ul className="comments-list">
        {renderComments()}
      </ul>
    </div>
  );
}
 
export default Comments;