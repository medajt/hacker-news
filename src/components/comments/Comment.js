import React from 'react';
import TimeConversion from '../TimeConversion';

function Comment(props) {

  return (
    <li key={props.id}>
      <div dangerouslySetInnerHTML={{ __html: props.text }} />
      <div className="item-info">
        <span className="author">by: {props.by}</span>
        <span className="comment-time">{TimeConversion(props.time)}</span>
      
        <div className="sub-comments">
          {
            props.kids ? 
              <div 
                className="show-more-btn"
                data-kids-id={props.kids}
                onClick={props.handleSubcomments}
              >
                show replays
              </div> 
            : 
              null
          }
          <ul className="subcomments-list">
          </ul>
        </div>
      </div>
    </li>
  );
}

export default Comment;